import java.util.*;

public class Palindrome{

	public static void main (String [] args) {
		String s = inputString();
		out(s);	
	}  
            
    public static String inputString ()
    {
    	System.out.println("Enter a string to check if it is a palindrome");
    	Scanner in = new Scanner(System.in);
        return in.nextLine();
    }
    
    
    public static boolean isPalindrome (String s) {
    	boolean p = true;
    	for ( int i = 0, k = s.length() - 1; i < k; i++, k-- )
    	{
    		if(s.charAt(i) != s.charAt(k))
    			p = false;		
	}
    	return p;
}
    
    public static void out (String s)
    {
        if(isPalindrome(s))
           System.out.println(s+" is a palindrome");
        else
           System.out.println(s+" is not a palindrome"); 
    	}
   
}