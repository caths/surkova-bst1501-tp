import java.io.*;

	public class lab2 {
		
		public static void main(String[] args) {
			Point3d[] a = output();
			if(compare(a)) {  
				computeArea(a);	
			}
			else 
			{System.out.println("Введите другие координаты");		
			}			
		}
    
		public static String file() {
			try{
				   FileInputStream fdata = new FileInputStream("C:/Users/Surkova/eclipse-workspace/lab2 point3d/points.txt");
				   BufferedReader rs = new BufferedReader(new InputStreamReader(fdata));
				   String coor;
				   coor = rs.readLine();	
					   coor = coor.replaceAll("\\s+","");			
				   return coor;
			} 
		 
				catch ( IOException e) {
				System.out.println("ERROR");
				return "";
			}			
		}
		
		  static Point3d InputValues() {
		    Scanner in = new Scanner(System.in);
		    double[] coord = new double[3];
		    for (int i=0; i<3; i++)
		    {
		      coord[i] = in.nextDouble();
		    }
		    return (new Point3d(coord[0],coord[1],coord[2]));
		 }
		   
		public static Point3d[] output() {    
			System.out.println("Заданные координаты: ");
			String o =  file();   
			double[] p = new double[3];
			Point3d[] data = new Point3d[3];
			for(int j =0;j<3;j++) {
				String[] co = coord(o);
				System.out.println("Координаты "+(j+1)+" точки : "+co[j]);
				p = point(co[j]);
				data[j] = new Point3d(p[0],p[1],p[2]);
							
			}return data;
		}
   
		public static String[] coord(String a)
		{
			String[] coord = new String[3];
			for (int i=0;i<3;i++){
				coord = a.split(";");
				}
			return coord;//  
		}
		
		public static double [] point(String a) {
			double [] cp = new double[3];
			for (int i=0; i<3;i++) { //      ,   
				String[] p = a.split(",");
				 cp[i] = Double.parseDouble(p[i]);
				}
			return cp;
		}
		
		public static boolean compare(Point3d[] p) {
			if (p[0].equals(p[1]) && p[1].equals(p[2]) && p[2].equals(p[0])) {
				return false ;}
			else { 
				return true;}
		}
	
		public static void  computeArea(Point3d[] t) {
			  double a = t[0].distanceTo(t[1]);
			  double b = t[1].distanceTo(t[2]);
			  double c = t[2].distanceTo(t[0]);
			  double s = (a+b+c)/2;
			  
			  double res = Math.sqrt(s*(s-a)*(s-b)*(s-c));
			  System.out.println("Результат: "+ res);
		}
}